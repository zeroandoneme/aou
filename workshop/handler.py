import json
import boto3
import uuid

def save_record(event, context):

    dynamo_db = boto3.resource('dynamodb', region_name='eu-west-2')

    table = dynamo_db.Table('demoTable')

    print(json.dumps(event))

    record = json.loads(event['body'])

    if "id" not in record:
        record["id"] = str(uuid.uuid1())

    table.put_item(
        Item=record
    )

    response = {
        "statusCode": 201,
        "headers" : { "Access-Control-Allow-Origin" : "*" },  # Required for CORS support to work
        "body": json.dumps(record)
    }

    return response


def get_record(event, context):
    dynamo_db = boto3.resource('dynamodb', region_name='eu-west-2')

    table = dynamo_db.Table('demoTable')

    result = table.scan()

    print(json.dumps(result))

    body = dict()
    
    if "Items" in result:
        body["data"] = result["Items"]
    else:
        body["data"] = []

    response = {
        "statusCode": 200,
        "headers" : { "Access-Control-Allow-Origin" : "*" },  # Required for CORS support to work
        "body": json.dumps(body)
    }

    return response
